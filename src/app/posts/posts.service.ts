import { Injectable } from '@angular/core';
import { Post } from './post.model';
import {Subject} from 'rxjs';
import { HttpClient } from '@angular/common/http';
import {map} from 'rxjs/operators'
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class PostsService {
  url = 'http://nodeangular-env.nbaans4ydm.us-east-2.elasticbeanstalk.com/api/posts/';
  private posts: Post[] = [];
  private postsUpdated = new Subject<{posts: Post[], postCount: number}>();

  constructor(private http: HttpClient, private router: Router) { }

  getPosts(postsPerPage: number, currentPage: number) {
    const queryParams = `?pagesize=${postsPerPage}&page=${currentPage}`
    this.http.get<{message: string, posts: any, maxPosts: number}>(this.url + queryParams).pipe(map(postData => {
      return {posts: postData.posts.map(post => {
        return {
          title: post.title,
          content: post.content,
          id: post._id,
          imagePath: post.imagePath,
          creator: post.creator
        };
      }), maxPosts: postData.maxPosts};
    }))
    .subscribe((transformedPostsData) => {
      console.log(transformedPostsData);
      this.posts = transformedPostsData.posts;
      //object that is emitting both the posts and the post count
      this.postsUpdated.next({posts: [...this.posts], postCount: transformedPostsData.maxPosts});
    });
  }

  getPostUpdateListener() {
    return this.postsUpdated.asObservable();
  }

  getPost(id: string){
    return this.http.get<{_id: string, title: string, content: string, imagePath: string, creator: string}>(this.url + id);
  }

  addPost(title: string, content: string, image: File) {
    //const post: Post = {id: null, title: title, content:  content};
    const postData = new FormData();
    postData.append("title", title);
    postData.append("content", content);
    postData.append("image", image, title);
    this.http.post<{message: string, post: Post}>(this.url, postData).subscribe((responseData) => {
      // const post: Post = {
      //   id: responseData.post.id,
      //   title: title,
      //   content: content,
      //   imagePath: responseData.post.imagePath
      // };
      // console.log(responseData.message);
      // this.posts.push(post);
      // this.postsUpdated.next([...this.posts]);
      this.router.navigate(["/"]);
    });
  }

  updatePost(id: string, title: string, content: string, image: File | string) {
   // const post: Post = {id: id, title: title, content: content, imagePath: null};
   let postData: Post | FormData;
    if(typeof(image) === 'object'){
      postData = new FormData();
      postData.append('id', id);
      postData.append('title', title);
      postData.append('content', content);
      postData.append('image', image, title);
    } else {
      postData = {id: id, title: title, content: content, imagePath: image, creator: null};
    }
    return this.http.put(this.url + id, postData).subscribe(res => {
      // console.log(res);
      // const updatedPosts = [...this.posts];
      // //to change the old post with new one in the array of posts
      // const oldPostIndex = updatedPosts.findIndex(p => p.id === id);
      // const post: Post = {
      //   id: id,
      //   title: title,
      //   content: content,
      //   imagePath: ""};
      // updatedPosts[oldPostIndex] = post;
      // this.posts = updatedPosts;
      // //event emitter for the posts
      // this.postsUpdated.next([...this.posts]);
      this.router.navigate(["/"]);
    });
  }

  deletePost(postId: string){
    return this.http.delete(this.url + postId);
  }

}
