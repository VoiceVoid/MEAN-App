
const Post = require('../models/post');

exports.createPost = (req,res,next)=>{
  const url = req.protocol + '://' + req.get("host");
  const post = new Post({
    title: req.body.title,
    content: req.body.content,
    imagePath: url + "/images/" + req.file.filename,
    creator: req.userData.userId
  });

  post.save().then(responseData=>{
    console.log(responseData);
    res.status(201).json({
    message: 'Post added successfully',
    post: {
       //NEWER WAY
       ...responseData,
      id: responseData._id,
      //OLD WAY
      // title: responseData.title,
      // content: responseData.content,
      // imagePath: responseData.imagePath
    }
    });
  })
  .catch(error => {
    res.status(500).json({
      message: 'Creating a post failed!'
    });
  })
}

exports.updatePost = (req,res, next)=>{
  console.log(req.file);
  //either it is the path of the image we already have
  let imagePath =req.body.imagePath;
  //or it is the new path of the picture
  if(req.file){
    const url = req.protocol + '://' + req.get("host");
    imagePath = url + "/images/" + req.file.filename;
  }
  const post = new Post({
    _id: req.body.id,
    title: req.body.title,
    content: req.body.content,
    imagePath: imagePath,
    creator: req.userData.userId
  })
  console.log(post);
  Post.updateOne({_id: req.params.id, creator: req.userData.userId}, post).then(result =>{
    console.log(result);
    if(result.n > 0){
      res.status(200).json({message: 'Update successfull'});
    } else {
      res.status(401).json({message: 'Not authorized!'});
    }
    console.log(result);
  })
  .catch(error =>{
    res.status(500).json({
      message: 'Couldnt update post!'
    })
  })
}

exports.getPosts = (req, res, next)=>{
  //localhost:4000/api/posts?pagesize=2&page=1&something=cool <= QUERY PARAMETER
  //console.log(req.query);
  const pageSize = +req.query.pagesize;
  const currentPage = +req.query.page;
  const postQuery = Post.find();
  let fetchedPosts;
  if(pageSize && currentPage) {
    postQuery
    .skip(pageSize * (currentPage - 1))
    .limit(pageSize);
  }
  postQuery.then(documents =>{
    fetchedPosts = documents;
    return Post.count();
  }).then(count => {
    res.status(200).json({
      message: "Posts fetched successfully!",
      posts: fetchedPosts,
      maxPosts: count
    })
  })
  .catch(error =>{
    res.status(500).json({
      message: 'Fetching posts failed!'
    })
  });
}

exports.getPost =  (req,res,next)=>{
  Post.findById(req.params.id).then(post =>{
    if(post){
      res.status(200).json(post);
    } else {
      res.status(404).json({message: 'Post not found'});
    }
  })
  .catch(error =>{
    res.status(500).json({
      message: 'Fetching post failed!'
    })
  });
}

exports.deletePost = (req, res, next)=>{
  Post.deleteOne({_id: req.params.id, creator: req.userData.userId}).then(result =>{
    console.log(result);
    if(result.n > 0){
      res.status(200).json({message: 'Deletion successfull'});
    } else {
      res.status(401).json({message: 'Not authorized!'});
    }
  })
  .catch(error =>{
    res.status(500).json({
      message: 'Deleting post failed!'
    })
  })
}
