const path = require('path');
const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');

const postsRoutes = require('./routes/posts');
const userRoutes = require('./routes/user');

const app = express();

//connecting to MongoDB
mongoose.connect("mongodb+srv://gregorslana:oCkXATlu1HoBJPpk@cluster0-bijob.mongodb.net/node-angular")
.then(()=>{
  console.log('Connected to database!');
})
.catch(()=>{
  console.log('Connection failed!');
});

//to parse json
app.use(bodyParser.json());

//to allow fetching images from image folder
app.use("/images", express.static(path.join("images")));

app.use((req,res,next)=>{
  res.setHeader('Access-Control-Allow-Origin', "*");
  res.setHeader('Access-Control-Allow-Headers', "Origin, X-Requested-With, content-type, Accept, Authorization");
  res.setHeader("Access-Control-Allow-Methods", "GET, POST, PATCH, PUT, DELETE, OPTIONS");
  next();
});

//for using routes in routes/posts.js file
app.use("/api/user", userRoutes);
app.use("/api/posts", postsRoutes);



module.exports = app;

