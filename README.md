# MEAN App

Using Express framework for backend. MongoDB for database, and Angular 6 for frontend.

Goal of this app is to upload an image using all of the CRUD methods to delete, edit, create and read posts.

Features of this app:
- using Authentication and Authorization
- File uploading and pagination
- lazy loading modules
- using interceptors and guards for capturing headers for authentication and authorization and to capture errors
- custom validator to only allow uploading images

Node.js, Express is hosted on Amazon ElasticBeanStalk, Angular app is hosted on S3. MongoDB is set up on MongoDB Atlas Cloud.

Link to a website:
http://mean-angular-nodeapp.s3-website.eu-central-1.amazonaws.com/auth/signup

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 6.0.0.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
