const express = require('express');
const Post = require('../models/post');

const router = express.Router();
const PostController = require('../controllers/posts')
const checkAuth = require('../middleware/check-auth')
const extractFile = require('../middleware/file')


//multer will try to find single file on property "image" request body
router.post("", checkAuth, extractFile ,PostController.createPost );

router.put('/:id',checkAuth ,extractFile ,PostController.updatePost)

router.get('' ,PostController.getPosts);

router.get('/:id',PostController.getPost)

router.delete('/:id',checkAuth ,PostController.deletePost);

module.exports = router;

