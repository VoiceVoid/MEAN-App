import { Component, OnInit, OnDestroy } from "@angular/core";
import { NgForm } from "@angular/forms";
import { AuthService } from "./auth.service";
import { Subscription } from "rxjs";
import { Router } from "@angular/router";

@Component({
  selector: "app-signup",
  templateUrl: "./signup.component.html",
  styleUrls: ["./signup.component.css"]
})
export class SignupComponent implements OnInit, OnDestroy {
  isLoading = false;
  private authStatusSub: Subscription;
  constructor(public service: AuthService, private router: Router) {}

  ngOnInit() {
    this.authStatusSub = this.service.getAuthStatusListener().subscribe( authStatus =>{
      this.isLoading = false;
    });
  }

  onSignup(form: NgForm) {
    if (form.invalid) {
      return;
    }
    // this.isLoading = true;
    this.service
      .createUser(form.value.email, form.value.password);

    console.log(form.value);
  }

  ngOnDestroy(){
    this.authStatusSub.unsubscribe();
  }
}
